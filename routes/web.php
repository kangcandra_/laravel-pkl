<?php

// menggunakan / mengimport LatihanController
use App\Http\Controllers\LatihanController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

// route basic
Route::get('about', function () {
    return view('tentang');
});

Route::get('profile', function () {
    $nama = "Candra H";
    // compact bertugas untuk mengirimkan var ke dalam view
    return view('pages.profile', compact('nama'));
});

// route parameter
Route::get('biodata/{nama}', function ($a) {
    return view('pages.biodata', compact('a'));
});

Route::get('order/{var1}/{var2}/{param3}', function ($a, $b, $c) {
    return view('pages.order', compact('a', 'b', 'c'));
});

// Route optional parameter
Route::get('pesan/{menu?}', function ($a = "silakan masukan pesanan") {
    return view('pages.pesan', compact('a'));
});

Route::get('pemesanan/{menu1?}/{menu2?}/{menu3?}', function ($a = "Silahkan Pesan Dulu", $b = null, $c = null) {
    return view('pages.pemesanan', compact('a', 'b', 'c'));
});

// pemanggilan route dengan controller
Route::get('latihan', [LatihanController::class, 'perkenalan']);
Route::get('latihan/{nama}/{alamat}/{umur}', [LatihanController::class, 'intro']);
Route::get('siswa', [LatihanController::class, 'siswa']);

Route::get('dosen', [LatihanController::class, 'skripsi']);
Route::get('perkalian/{perkalian}/{nama}', [LatihanController::class, 'perkalian']);

Route::get('hitung-bmi/{bb}/{tb}', [LatihanController::class, 'BMI']);

// route post
Route::get('post', [PostController::class, 'tampil']);
Route::get('post/{id}', [PostController::class, 'search']);
Route::get('post/judul/{title}', [PostController::class, 'search_title']);
Route::get('post/edit/{id}/{title}/{content}', [PostController::class, 'edit']);
Route::get('post/tambah/{title}/{content}', [PostController::class, 'tambah']);
Route::get('post/delete/{id}', [PostController::class, 'hapus']);
