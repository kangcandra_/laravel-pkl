<?php

namespace App\Http\Controllers;

class LatihanController extends Controller
{
    public function perkenalan()
    {
        // passing variable to view
        $nama = "Candra";
        $alamat = "Bandung";
        $umur = 23;

        return view('pages.perkenalan', compact('nama', 'alamat', 'umur'));
    }

    public function intro($nama, $alamat, $umur)
    {
        return view('pages.perkenalan', compact('nama', 'alamat', 'umur'));
    }

    public function siswa()
    {
        $a = [
            array('id' => 1, 'name' => 'Dadang', 'age' => 15,
                'hobi' => [
                    'Membaca Novel', 'Push Rank', 'Bernyanyi',
                ]),
            array('id' => 2, 'name' => 'Dudung', 'age' => 18,
                'hobi' =>
                [
                    'Renang', 'Futsal', 'Mancing', 'Turu',
                ]),
        ];
        // dump data
        // dd($a);
        return view('pages.siswa', ['students' => $a]);
    }

    public function skripsi()
    {
        $dosen = [
            ['nama' => 'Yusuf Bachtiar', 'mata_kuliah' => 'Pemrograman Mobile', 'mahasiswa' =>
                [
                    ['nama' => 'Muhammad Sholeh', 'nilai' => 78],
                    ['nama' => 'Jujun Junaedi', 'nilai' => 85],
                    ['nama' => 'Mamat Alkatiri', 'nilai' => 90],
                ],
            ],
            ['nama' => 'Yaris Riyadi', 'mata_kuliah' => 'Pemrograman Web', 'mahasiswa' =>
                [
                    ['nama' => 'Alfred McTomminay', 'nilai' => 67],
                    ['nama' => 'Bruno Kasmir', 'nilai' => 90],
                ],
            ],
        ];
        // dd($dosen);
        return view('pages.dosen', compact('dosen'));
    }

    public function perkalian($perkalian, $nama)
    {
        $c = 1 + 5;
        $hasil = $c * $perkalian;
        return view('perkalian', compact('hasil', 'nama'));
    }

    public function BMI($bb, $tb)
    {
        $hitung = $bb / (($tb / 100) * ($tb / 100));
        $status = "";
        if ($hitung >= 27.1) {
            $status = "Gemuk, kelebihan berat badan tingkat berat";
        } else if ($hitung >= 25.1 && $hitung <= 27.0) {
            $status = "Gemuk, kelebihan berat badan tingkat ringan";
        } else if ($hitung >= 18.5 && $hitung < 25.0) {
            $status = "Normal";
        } else if ($hitung < 18.4 && $hitung >= 17.1) {
            $status = "Kurus, kekurangan berat badan ringan";
        } else if ($hitung < 17.0) {
            $status = "Kurus, kekurangan berat badan berat";
        }
        return view('hitung-bmi', compact('hitung', 'bb', 'tb', 'status'));
    }

}
