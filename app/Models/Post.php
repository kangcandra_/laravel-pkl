<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    // model singular -> satu
    // migration plural -> jamak / banyak
    // memberi akses model Post digunakan untuk table atau migration post
    protected $table = 'post]';

}
