<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    Tinggi Badan : <b>{{ $tb }}</b>cm <br>
    Berat Badan : <b>{{ $bb }}</b>kg <br>
    Hasil BMI : <b>{{ $hitung }}</b> <br>
    Status Kesehatan : <b>{{ $status }}</b>

    <p>Di bawah 17,0 = Kurus, kekurangan berat badan berat <br>
        ·17,0 - 18,4 = Kurus, kekurangan berat badan ringan <br>
        18,5 – 25,0 = Normal <br>
        25,1 – 27,0 = Gemuk, kelebihan berat badan tingkat ringan <br>
        diatas 27,1 = Gemuk, kelebihan berat badan tingkat berat</p>
</body>

</html>
