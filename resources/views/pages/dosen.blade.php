<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <fieldset>
        <legend>
            Data Dosen
        </legend>

        @foreach ($dosen as $data)
            @php
                $total = 0;
            @endphp
            Nama Dosen: {{ $data['nama'] }} <br>
            Mata Kuliah : {{ $data['mata_kuliah'] }} <br>
            Daftar Mahasiswa :
            @foreach ($data['mahasiswa'] as $mhs)
                <li>{{ $mhs['nama'] }} <br>
                    Nilai Skripsi : {{ $mhs['nilai'] }}
                </li>
                <hr>
                @php $total += $mhs['nilai'] @endphp
            @endforeach
            @php $rata_rata = $total / count($data['mahasiswa']) @endphp
            Total Nilai Mahasiswa : {{ $total }} <br>
            Rata-rata nilai Skripsi Mahasiswa bimbingan <b>{{ $data['nama'] }}</b> :
            {{ $rata_rata }}
            <b>
                <hr style="border: 1px  dashed red">
            </b>
        @endforeach
    </fieldset>
</body>

</html>
