<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <fieldset>
        <legend>Pemesanan</legend>
        @if ($a == null)
            <strong> {{ $a }} </strong>
        @elseif($a == 'mie goreng')
            Makanan : <strong>{{ $a }}</strong> <br>
            Harga : Rp. 5000
        @elseif($a == 'seblak')
            Makanan : <strong>{{ $a }}</strong> <br>
            Harga : Rp. 7500
        @elseif($a == 'nasi padang')
            Makanan : <strong>{{ $a }}</strong> <br>
            Harga : Rp. 15000
        @else
            Makanan : <strong>{{ $a }}</strong> <br>
        @endif
        <hr>

        @if ($b == null)
            <strong> {{ $b }} </strong>
        @elseif($b == 'teh')
            Minuman : <strong>{{ $b }}</strong> <br>
            Harga : Rp. 5000
            <hr>
        @elseif($b == 'kopi')
            Minuman : <strong>{{ $b }}</strong> <br>
            Harga : Rp. 7500
            <hr>
        @elseif($b == 'jus')
            Minuman : <strong>{{ $b }}</strong> <br>
            Harga : Rp. 10000
            <hr>
        @else
            Minuman : <strong>{{ $b }}</strong> <br>
            <hr>
        @endif

        @if ($c == null)
            <strong> {{ $c }} </strong>
        @elseif($c == 'kecil')
            Porsi : <strong>{{ $c }}</strong> <br>
            Tambah Harga : Rp. 2500
            <hr>
        @elseif($c == 'sedang')
            Porsi : <strong>{{ $c }}</strong> <br>
            Tambah Harga : Rp. 5000
            <hr>
        @elseif($c == 'besar')
            Porsi : <strong>{{ $c }}</strong> <br>
            Tambah Harga : Rp. 10000
            <hr>
        @else
            porsi : <strong>{{ $c }}</strong>
            <hr>
        @endif
    </fieldset>
</body>

</html>
